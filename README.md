# Infrastructure guidelines for subgrantees

This is a guide on the GRNET-GitLab infrastructure hosting eSSIF-Lab projects under development.

## Project hosting

Projects will be hosted in [GRNET-Gitlab](https://gitlab.grnet.gr/users/sign_in),
as self-hosted GitLab instance of GRNET.

eSSIF-Lab subgrantees have been given external GitLab-GRNET accounts, which have
been organized into corresponding GitLab groups. Groups participating in BOC1 and IOC1
reside under the [business](https://gitlab.grnet.gr/essif-lab/business)
and [infrastructure](https://gitlab.grnet.gr/essif-lab/infrastructure) groups respectively.  

Among each group's projects (GitLab repositories), some have been marked as INTERNAL,
that is, by default visible to logged-in GitLab-GRNET users. This is attained by
automatically inviting all subgrantees as REPORTER to other group’s projects.
More specifically, subgrantees have read access to codebases of other groups and
write access to issues, so that they can leave comments to other groups and open
discussions for the purpose of promoting collaboration among them.  

Each group member has elevated OWNER rights over their own projects. GRNET stuff
retains also owner rights over all projects for administrative reasons.

In order for reviewers to easily navigate projects, groups must conform to a uniform convention and observe this structure:

### Group structure

```
[group]/  
├── [project]_project_summary/
├── [project]/
├── deliverables/
│   ...
...
```

where `[group]` is the name of the group and `[project]` is the project’s acronym.  
In the above hierarchy:

- `[project]_project_summary`: summary for quick review
- `[project]`: is intended for the actual codebase
- `deliverables`: should contain any other documents requested by eSSIF

Projects are by default visible to other groups. Technically speaking, this means that all projects have been marked as INTERNAL and all subgrantees have been invited as REPORTER to them. A reporter van view the code, open an issue or leave comments to it, but they cannot commit changes or even clone the project.  Internal projects have become visible irrespective of call, that is, BOC groups can now access IOC projects and vice versa.

### References

- [Gilab subgoups](https://docs.gitlab.com/ee/user/group/subgroups/)
- [Gitlab projects](https://docs.gitlab.com/ee/user/project/)
- [Project permissions](https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions)


## CI/CD

[GitLab CI/CD](https://docs.gitlab.com/ee/ci/) is a built in tool that facilitates
continuous methodologies (_Continuous Integration_, _Delivery_ and _Deployment_).
It allows for quick iterations and agile development without need of third-party
tools integration.

### `.gitlab-ci.yml`

The scripts comprising the CI/CD pipeline must be determined in a configuration
file called `.gitlab-ci.yml`, residing in the root path of the `master` branch of
each repo.
Adhering to the directives given in that file, GitLab detects and runs
scripts with a built-in tool called the [GitLab Runner](https://docs.gitlab.com/runner/),
thought of as perfectly emulating your terminal. The pipeline will be triggered upon
every push to any branch of your repo.

Scripts may freely be organized in any sequence suitable for your specific needs
and in accordance with the builds, tests and deployments to take place. In `.gitlab-ci.yml`
you can define dependencies and [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/README.html),
as well as specify which commands are to be run in sequence or parallel, automatically or
to be triggered manually. You can even dictate what should be done in case of
specific conditions encountered, e.g., when a process succeeds or fails.

Scripts are grouped in **jobs**, appearing as first-level attributes
with at least one `script` clause. The `before_script`, resp. `after_script`
clauses define commands to be run before, resp. after each job; if defined per
job, they overwrite any global configuration. Scripts specified in
`before_script` are concatenated with any scripts inside the main block and
executed together in a single shell; the `after_script` defines instead a
command to be run in a new shell, without access to any changes introduced previously.

``` yaml
default:
  before_script:
    - global before script

job:
  before_script:
    - execute this instead of global before script
  script:
    - my command
  after_script:
    - execute this after my script
```

The `stages` clause is used to determine the ordering of jobs’ execution
according to these rules:

1. Jobs falling under the same stage are run in parallel
2. Jobs of the next stage are run after all jobs of the previous stage
have successfully completed  

Attachment to a specific stage is defined per job by using the `stage` clause.
A very generic, albeit not obligatory `.gitlab-ci.yml` structure would look
as follows:

```yaml
stages:
  - test
  - build
  - deploy

test backend:
  ...
  stage: test
  script:
    - ...

test ui:
  ...
  stage: test
  script:
  - ...

some build job:
  ...
  stage: build
  script:
    - ...

some deploy job:
  ...
  stage: deploy
  script:
    - ...
```

#### References

- Complete [`.gitlac-ci.yml` reference](https://docs.gitlab.com/ee/ci/yaml/README.html)
on how the configure the CI/CD pipeline
- Exposition of basic [CI/CD pipeline architectures](https://docs.gitlab.com/ee/ci/pipelines/pipeline_architectures.html)
- GitLab [CI/CD examples](https://docs.gitlab.com/ee/ci/examples/README.html)
for a variety of stacks

### Example

Suppose your project consists of two hypothetical services called `backend` and `ui`,
built with *Django*, resp. *React*, having the following structure:

```
your-project
    ├── backend
    │   ├── ...
    │   ...
    │   ├── requirements.txt
    │   ├── requirements-dev.txt
    │   └── test.sh
    ├── ui
    │   ├── ...
    │   └── cypress
    │       ├── ...
    │       └── ...
    ...
```

We will configure the CI/CD pipeline by including an appropriate `.gitlab-ci.yml`.

```yaml
variables:
  REG_URL: "registry.docker.[...].com"
  ...

stages:
  - test
  - build
  - deploy
```

#### Continuous Integration

_Continuous Integration_ consists of a set of scripts for automatically building
and testing your application upon every push, which dramatically increases the
chance of catching bugs at an early stage.

```yaml
test backend:
  image: $REG_URL/your-project-backend-base:latest
  stage: test
  variables:
    YOUR_PROJECT_DEBUG: 'True'
    YOUR_PROJECT_HOSTS: 'localhost'
    ...
  before_script:
    - apt-get update
    - apt-get install -y python-pip
    ...
    - cd your-project-backend
    - pip install -r requirements.txt
    - pip install -r requirements-dev.txt
  script:
    - ./test.sh

test ui:
  image: $REG_URL/your-project-frontend-base:latest
  stage: test
  before_script:
    - cd ui
    - yarn
  script:
    - yarn test --coverage
```

```yaml
build:
  image: docker:19.03.1
  stage: build
  before_script:
    - docker login -u $DOCKER_REGISTRY_USER -p $DOCKER_REGISTRY_PASSWORD $DOCKER_REGISTRY_URL
  script:
    - docker build --build-arg [...] backend/
    - docker build --build-arg [...] ui/
    ...
  after_script:
    - docker logout $DOCKER_REGISTRY_URL
    ...
```

#### Continuous Delivery/Deployment

In _Continuous Delivery_ your application must not only be built and tested
upon every push to the codebase, but also ready to be deployed, although actual
deployment proceeds by manual triggering. _Continuous Deployment_ is when
deployment must take place without human intervention.


```yaml
deploy:
  image: [...]
  stage: deploy
  when: manual
  before_script:
    - [...]
  script:
    - [...]
  after_script:
    - [...]

```

## Documentation and static website

Except for using your preferred documentation pipeline, you are also prompted
to include your project's description and documentation in a static website,
which can be easily deployed under **https://essif-lab.pages.grnet.gr**. Note that
this requires a separate repository within your workspace (that is, a dedicated
Gitlab project subordinate to your subgroup). Content and configuration
files concerning deployment must be included there.

The proposed tool for building static websites from `.md` files is
[Docusaurus 2](https://v2.docusaurus.io/).

### Building a static website with Docusaurus

You must have installed [Node.js](https://nodejs.org/en/download/) >= 10.9.0
and [Yarn](https://classic.yarnpkg.com/en/) >= 1.5. A complete minimal example
may be found [here](https://gitlab.grnet.gr/essif-lab/essif-lab/-/tree/feature-docusaurus).

#### Installation and setup

Run

```
$ npx @docusaurus/init@next init [docurepo] classic
```

locally in order to scaffold a basic skeleton website inside the newly created
reopsitory `[docurepo]`:

```
[docurepo]/
    ├── blog
    │   ├── ...
    │   ...
    ...
    ├── docs
    │   ├── doc1.md
    │   ...
    │   └── mdx.md
    ├── docusaurus.config.js
    ├── package.json
    ├── ...
    ├── sidebars.js
    ├── src
    │   ├── css
    │   │   └── custom.css
    │   └── pages
    │       ├── index.js
    │       └── styles.module.css
    └── static
        └── img
            ├── ...
            ...
```

Enter the directory and run

```
$ yarn
```

to install packages. Make it into a git repository and add the remote you
will be pushing to:

```
$ cd [docurepo]
$ git init
$ git add .
$ git commit -am "Basic website structure"
$ git remote add origin remote [remote repo URL]
```

#### Local development

You can run a local development server with

```
$ yarn start
```

Most changes will be reflected live without having to restart it. If you face an
error similar to "There are no scenarios, must have at least one",
refer [here](https://github.com/yarnpkg/yarn/issues/2821) for a correct
re-installation of `yarn`.

#### Writing docs guide

See the [official documentation](https://v2.docusaurus.io/docs/creating-pages) for a complete reference.

Content must be put inside `docs/*.md` files. Each such file must define the
following attributes at its very beginning:

- `id`, by which the file will be referred to across the repo

- `title`, appearing at the top of the corresponding page

- `sidebar_label`, name appearing in the sidebar

The `sidebars.js` file will use these attributes to control distribution of content
among sections in a self-explanatory way. Subsections inside a `.md` file,
i.e, tagged with _##_, will appear at the right part of the corresponding page.

Images must be put inside `static/` and referred to with their absolute URLs.


### Deployment of static website

This is as easy as including a GitLab job and pushing to the appropriate branch.
Simply put a `.gitlab-ci.yml` file at the top of your repo with the following
content:

```
image: node:latest

cache:
  untracked: true
  paths:
    - node_modules/

before_script:
  - yarn

pages:
  script:
    - STAGING=true  yarn build
    - rm -rf public
    - mv build public
  artifacts:
    paths:
      - public
  only:
    - master
```

Whenever you push to the `master` branch, changes will be automatically deployed
at

https://essif-lab.pages.grnet.gr/your-repo-name/

provided that you repo is called `eSSIF-Lab <your-repo-name>` under the
[eSSIF-Lab](https://gitlab.grnet.gr/essif-lab) project. If you want a different
branch to be deployed, change `master` inside `.gitlab-ci.yml` to anything
you'd like.
